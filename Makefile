CC=gcc
ASM=nasm
CFLAGS=-std=gnu99 -Iinclude 
AFLAGS=-felf64

all: sepia

sepia: main.o sse.o image_parser.o image_filter.o
	$(CC) $(CFLAGS) main.o sse.o image_parser.o image_filter.o -o sepia

main.o:
	$(CC) $(CFLAGS) -c src/main.c

sse.o:
	$(ASM) $(AFLAGS) -o sse.o src/sse.asm

image_parser.o:
	$(CC) $(CFLAGS) -c src/image_parser.c

image_filter.o:
	$(CC) $(CFLAGS) -c src/image_filter.c
clean:
	rm -rf sepia *.o
