#pragma once

#include "image_parser.h"

void sse_sepia(struct Image* image);

void naive_sepia(struct Image* image);